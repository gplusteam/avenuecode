package com.avenuecode.avenuecodetest.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.avenuecodetest.dao.ProductRepository;
import com.avenuecode.avenuecodetest.models.Product;

@Service
public class ProductBusiness {

    @Autowired
    private ProductRepository repository;

    public Iterable<Product> findAll() {
        return repository.findAll();
    }

    public Product create(String name, Optional<String> description, Optional<Long> idParent) {
        Product entity = new Product();

        if (idParent.isPresent()) {

            Product parent = repository.findOne(idParent.get());
            if (parent != null) {
                entity.setParent(parent);
            }
        }

        entity.setName(name);
        entity.setDescription(description.orElse(null));

        entity = repository.save(entity);

        return entity;
    }

    public Product update(Long id, Optional<String> name, Optional<String> description, Optional<Long> idParent) {
        if (idParent.isPresent()) {
            Product parent = repository.findOne(idParent.get());
            
            if (parent != null) {
                repository.updateParentById(id, parent);
            }
            
        }
        
        if (name.isPresent()) {
            repository.updateById(id, name.get(), description.orElse(null));
        }
        
        return repository.findOne(id);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public Product findById(Long id) {
        return repository.findOne(id);
    }
}
