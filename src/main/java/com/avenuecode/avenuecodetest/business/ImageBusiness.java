package com.avenuecode.avenuecodetest.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.avenuecodetest.dao.ImageRepository;
import com.avenuecode.avenuecodetest.dao.ProductRepository;
import com.avenuecode.avenuecodetest.models.Image;
import com.avenuecode.avenuecodetest.models.Product;

@Service
public class ImageBusiness {

    @Autowired
    private ImageRepository repository;
    
    @Autowired
    private ProductRepository productRepository;


    public Image create(String type, Long idProduct) {
        Image entity = new Image();

        Product product = productRepository.findOne(idProduct);
        if (product != null) {
            entity.setProduct(product);
        }

        entity.setType(type);

        entity = repository.save(entity);

        return entity;
    }

    public Image update(Long id, Optional<String> type, Optional<Long> idProduct) {
        if (idProduct.isPresent()) {
            Product product = productRepository.findOne(idProduct.get());
            
            if (product != null) {
                repository.updateProductById(id, product);
            }
        }
        
        if (type.isPresent()) {
            repository.updateById(id, type.get());
        }
        
        return repository.findOne(id);
    }

    public void delete(Long id) {
        repository.delete(id);
    }
}
