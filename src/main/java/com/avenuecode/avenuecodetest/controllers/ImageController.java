package com.avenuecode.avenuecodetest.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.avenuecode.avenuecodetest.business.ImageBusiness;
import com.avenuecode.avenuecodetest.models.Image;
import com.avenuecode.avenuecodetest.models.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.JsonViewModule;
import com.monitorjbl.json.Match;

/**
 * Class ProductController
 */
@Controller
@RequestMapping("/image")
public class ImageController {

    // Wire the UserDao used inside this controller.
    @Autowired
    private ImageBusiness business;
    
    @RequestMapping(value = "/create", produces = "application/json")
    @ResponseBody
    public String create(@RequestParam("url") String url,  @RequestParam("idProduct") Long idProduct) throws JsonProcessingException {
        Image result = business.create(url, idProduct);
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        return mapper.writeValueAsString(JsonView.with(result).onClass(Image.class, Match.match().exclude("product")));
    }
    
    @RequestMapping(value = "/update/{id}", produces = "application/json")
    @ResponseBody
    public String create(@PathVariable("id") Long id,  @RequestParam("url") Optional<String> url, @RequestParam("idParent") Optional<Long> idProduct) throws JsonProcessingException {
        Image result = business.update(id, url, idProduct);
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        return mapper.writeValueAsString(JsonView.with(result).onClass(Image.class, Match.match().exclude("product")));
    }
    
    @RequestMapping(value = "/delete/{id}", produces = "application/json")
    @ResponseBody
    public String create(@PathVariable("id") Long id) throws JsonProcessingException {
        business.delete(id);
        
        return "{\"ok\": 1}";
    }
}
