package com.avenuecode.avenuecodetest.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.avenuecode.avenuecodetest.business.ProductBusiness;
import com.avenuecode.avenuecodetest.models.Image;
import com.avenuecode.avenuecodetest.models.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.JsonViewModule;
import com.monitorjbl.json.Match;

/**
 * Class ProductController
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    // Wire the UserDao used inside this controller.
    @Autowired
    private ProductBusiness business;
    
    @RequestMapping(value = "/get/all", produces = "application/json")
    @ResponseBody
    public String list(@RequestParam("showChildren") Optional<Boolean> showChildren, @RequestParam("showImages") Optional<Boolean> showImages) throws JsonProcessingException {
        Iterable<Product> result = business.findAll();
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        JsonView<?> view = JsonView.with(result);
        List<String> productExclusion = new ArrayList<String>(Arrays.asList("parent"));
        List<String> imageExclusion = new ArrayList<String>(Arrays.asList("product"));
        
        if (showChildren.orElse(false) == false) {
            productExclusion.add("children");
        }
        
        if (showImages.orElse(false) == false) {
            productExclusion.add("images");
        }
        
        view.onClass(Product.class, Match.match().exclude(productExclusion.toArray(new String[0])));
        view.onClass(Image.class, Match.match().exclude(imageExclusion.toArray(new String[0])));
        
        return mapper.writeValueAsString(view);
    }
    
    @RequestMapping(value = "/get/{id}", produces = "application/json")
    @ResponseBody
    public String list(@PathVariable("id") Long id, @RequestParam("showChildren") Optional<Boolean> showChildren, @RequestParam("setChildren") Optional<List<Long>> setChildren, @RequestParam("showImages") Optional<Boolean> showImages, @RequestParam("setImages") Optional<List<Long>> setImages) throws JsonProcessingException {
        Product result = business.findById(id);
        
        if (setChildren.isPresent()) {
            if (result.getChildren() != null) {
                result.setChildren(result.getChildren().stream().filter((child) -> setChildren.get().contains(child.getId())).collect(Collectors.toList()));
            }
        }
        
        if (setImages.isPresent()) {
            if (result.getImages() != null) {
                result.setImages(result.getImages().stream().filter((image) -> setImages.get().contains(image.getId())).collect(Collectors.toList()));
            }
        }
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        JsonView<?> view = JsonView.with(result);
        List<String> productExclusion = new ArrayList<String>(Arrays.asList("parent"));
        List<String> imageExclusion = new ArrayList<String>(Arrays.asList("product"));
        
        if (showChildren.orElse(false) == false) {
            productExclusion.add("children");
        }
        
        if (showImages.orElse(false) == false) {
            productExclusion.add("images");
        }
        
        view.onClass(Product.class, Match.match().exclude(productExclusion.toArray(new String[0])));
        view.onClass(Image.class, Match.match().exclude(imageExclusion.toArray(new String[0])));
        
        return mapper.writeValueAsString(view);
    }

    @RequestMapping(value = "/child/get/{id}", produces = "application/json")
    @ResponseBody
    public String listChlid(@PathVariable("id") Long id) throws JsonProcessingException {
        List<Product> result = new ArrayList<Product>();
        Product product = business.findById(id);
        
        if (product != null && product.getChildren() != null) {
            result = product.getChildren();
        }
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        JsonView<?> view = JsonView.with(result);
        List<String> productExclusion = new ArrayList<String>(Arrays.asList("parent", "children", "images"));
        List<String> imageExclusion = new ArrayList<String>(Arrays.asList("product"));
        
        view.onClass(Product.class, Match.match().exclude(productExclusion.toArray(new String[0])));
        view.onClass(Image.class, Match.match().exclude(imageExclusion.toArray(new String[0])));
        
        return mapper.writeValueAsString(view);
    }
    
    @RequestMapping(value = "/image/get/{id}", produces = "application/json")
    @ResponseBody
    public String listImage(@PathVariable("id") Long id) throws JsonProcessingException {
        List<Image> result = new ArrayList<Image>();
        Product product = business.findById(id);
        
        if (product != null && product.getImages() != null) {
            result = product.getImages();
        }
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        JsonView<?> view = JsonView.with(result);
        List<String> productExclusion = new ArrayList<String>(Arrays.asList("parent", "children", "images"));
        List<String> imageExclusion = new ArrayList<String>(Arrays.asList("product"));
        
        view.onClass(Product.class, Match.match().exclude(productExclusion.toArray(new String[0])));
        view.onClass(Image.class, Match.match().exclude(imageExclusion.toArray(new String[0])));
        
        return mapper.writeValueAsString(view);
    }
    
    @RequestMapping(value = "/create", produces = "application/json")
    @ResponseBody
    public String create(@RequestParam("name") String name,  @RequestParam("description") Optional<String> description, @RequestParam("idParent") Optional<Long> idParent) throws JsonProcessingException {
        Product result = business.create(name, description, idParent);
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        return mapper.writeValueAsString(JsonView.with(result).onClass(Product.class, Match.match().exclude("parent", "children", "images")));
    }
    
    @RequestMapping(value = "/update/{id}", produces = "application/json")
    @ResponseBody
    public String create(@PathVariable("id") Long id,  @RequestParam("name") Optional<String> name, @RequestParam("description") Optional<String> description, @RequestParam("idParent") Optional<Long> idParent) throws JsonProcessingException {
        Product result = business.update(id, name, description, idParent);
        
        ObjectMapper mapper = new ObjectMapper().registerModule(new JsonViewModule());
        
        return mapper.writeValueAsString(JsonView.with(result).onClass(Product.class, Match.match().exclude("parent", "children", "images")));
    }
    
    @RequestMapping(value = "/delete/{id}", produces = "application/json")
    @ResponseBody
    public String create(@PathVariable("id") Long id) throws JsonProcessingException {
        business.delete(id);
        
        return "{\"ok\": 1}";
    }
}
