package com.avenuecode.avenuecodetest.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.avenuecode.avenuecodetest.models.Image;
import com.avenuecode.avenuecodetest.models.Product;

@Repository
@Transactional
public interface ImageRepository extends JpaRepository<Image, Long> {
 
    @Modifying
    @Query("UPDATE Image i SET i.type = ?2 WHERE i.id = ?1")
    void updateById(Long id, String url);

    @Modifying
    @Query("UPDATE Image i SET i.product = ?2 WHERE i.id = ?1")
    void updateProductById(Long id, Product product);
}
