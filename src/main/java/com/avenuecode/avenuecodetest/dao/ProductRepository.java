package com.avenuecode.avenuecodetest.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.avenuecode.avenuecodetest.models.Product;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Long> {
 
    @Modifying
    @Query("UPDATE Product p SET p.name = ?2, p.description = ?3 WHERE p.id = ?1")
    void updateById(Long id, String name, String description);

    @Modifying
    @Query("UPDATE Product p SET p.parent = ?2 WHERE p.id = ?1")
    void updateParentById(Long id, Product parent);
}
