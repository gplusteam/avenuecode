INSERT INTO products (id, name, parent_product_id) VALUES
	(1, 'Product 1.0', null),
	(2, 'Product 1.1', 1),
	(3, 'Product 1.2', 1),
	(4, 'Product 1.3', 1),
	
	(5, 'Product 2.0', null),
	(6, 'Product 2.1', 5),
	(7, 'Product 2.2', 5),
	(8, 'Product 2.3', 5);
	
	
INSERT INTO images (id, type, product_id) VALUES
	(1, 'type_01', 1),
	(2, 'type_02', 1),
	(3, 'type_02', 1),
	(4, 'type_04', 1),
	
	(5, 'type_11', 2),
	(6, 'type_12', 2),
	(7, 'type_13', 2),
	(8, 'type_14', 2);
	
	