# Usage

## 1 Create, update and delete products
 + <http://localhost:8080/product/create?name=test>
 + <http://localhost:8080/product/delete/3>
 + <http://localhost:8080/product/update/1?name=Product_Mater>
 
## 2 Create, update and delete images
 + <http://localhost:8080/image/create?url=type_05&idProduct=1>
 + <http://localhost:8080/image/update/9?url=type_05_FIXED>
 + <http://localhost:8080/image/delete/9>

## 3 Get all products excluding relationships (child products, images) 
 + <http://localhost:8080/product/get/all>

## 4 Get all products including specified relationships (child product and/or images) 
 + <http://localhost:8080/product/get/all?showChildren=true>
 + <http://localhost:8080/product/get/all?showImages=true>
 + <http://localhost:8080/product/get/all?showChildren=true&showImages=true>
  
## 5 Same as 3 using specific product identity 
 + <http://localhost:8080/product/get/1>
 
## 6 Same as 4 using specific product identity 
 + <http://localhost:8080/product/get/1?showChildren=true>
 + <http://localhost:8080/product/get/1?showImages=true>
 + <http://localhost:8080/product/get/1?showChildren=true&showImages=true>
 
## 7 Get set of child products for specific product 
 + <http://localhost:8080/product/child/get/1?setChildren=2,3>
 + <http://localhost:8080/product/get/1?showChildren=true&setChildren=2,3>

## 8 Get set of images for specific product
 + <http://localhost:8080/product/image/get/1>
 + <http://localhost:8080/product/get/1?showImages=true&setImages=1,4>


### Build and run

#### Configurations

Open the `application.properties` file and set your own configurations for the
database connection.

#### Prerequisites

- Java 8
- Maven 3

#### From terminal

Go on the project's root folder, then type:

    $ mvn spring-boot:run